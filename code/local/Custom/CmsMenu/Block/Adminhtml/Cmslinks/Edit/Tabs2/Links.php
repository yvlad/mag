<?php

class Custom_CmsMenu_Block_Adminhtml_Cmslinks_Edit_Tabs_Links extends Mage_Adminhtml_Block_Widget_Grid
{
    
    protected function _prepareCollection() 
    {
        $collection = Mage::getModel( 'customcmsmenu/cmslinks' )->getCollection(); //Получаем модель коллекции
        $this->setCollection($collection); //Устанавливаем коллекцию
        return parent::_prepareCollection();
    }
    
    protected function _prepareColumns() 
    {
        
        //Создаем колонки 
        $helper = Mage::helper( 'customcmsmenu' );
        
        $this->addColumn( 'id', array(
            'header' => $helper->__( 'ID' ),
            'index'  => 'links_id',
            'type'   => 'text'
        ));

        $this->addColumn( 'url_key', array(
            'header' => $helper->__( 'URL Key' ),
            'index'  => 'url_key',
            'type'   => 'text',
        ));
        
        $this->addColumn( 'type', array(
            'header' => $helper->__( 'Type' ),
            'index'  => 'type',
            'type'    => 'options',
            'options' => array(
                '0' => 'Disabled',
                '1' => 'Enabled'
            )
        ));
         
        $this->addColumn( 'position', array(
            'header'  => $helper->__( 'Position' ),
            'index'   => 'position',
            'type'    => 'text',
            
        ));

        return parent::_prepareColumns();
        
    }
}