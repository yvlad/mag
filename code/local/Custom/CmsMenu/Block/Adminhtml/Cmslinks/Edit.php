<?php

class Custom_CmsMenu_Block_Adminhtml_Cmslinks_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    protected function _construct() {

        $this->_blockGroup = 'customcmslinks';
        $this->_controller = 'adminhtml_cmslinks';
        
//        $this->addButton( 'addmenulink', array(
//            'label'   => Mage::helper( 'customcmsmenu' )->__( 'Add Menu Link' ),
//            'onclick' => 'addMenuLink()',
//            'class'   => 'addMenu'
//        ), -100);
//        
//        $this->_formScripts[] = 
//                 "function addMenuLink(){
//                     editForm.submit($('edit_form').action+'link');
//                }";
                
                
    }
    
    public function getHeaderText() {
        $helper = Mage::helper('customcmsmenu');//Получаем хелпер
        $model  = Mage::registry('current_cmslinks');// Получаем из реестра current_cmsmenu глоб переменную
        
        if($model->getId()){
            return $helper->__("Edit Link item '%s'", $this->escapeHtml($model->getLabel()));//Получаем вывод в шапке редактирования меню
        }else{
            return $helper->__("New Link");//Получаем вывод в шапке Добавления меню
        }
    }
}