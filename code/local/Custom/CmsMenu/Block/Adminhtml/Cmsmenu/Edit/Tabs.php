<?php

class Custom_CmsMenu_Block_Adminhtml_Cmsmenu_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    
    public function _construct() 
    {
        $helper = Mage::helper('customcmsmenu');
        
        parent::_construct();
        $this->setId('cmsmenu_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle($helper->__('Menu Information'));
    }
    
    protected function _prepareLayout()
    {
        $helper = Mage::helper('customcmsmenu');
        
        $this->addTab('general_section', array(
           'label'    => $helper->__('General Information'),
            'title'   => $helper->__('General Information'),
            'content' => $this->getLayout()->createBlock('customcmsmenu/adminhtml_cmsmenu_edit_tabs_general')->toHtml(),
        ));
            
        $this->addTab('links_management', array(
            'label'   => $helper->__('Links Management'),
            'title'   => $helper->__('Links Management'),
            'content' => $this->getLayout()->createBlock('customcmsmenu/adminhtml_cmsmenu_edit_tabs_links')->toHtml()
        ));
        
    }
}