<?php

/**
 * All tabs must implement tab interface and extend 4 methods:
 * getTabLabel(), getTabTitle(), canShowTab(), isHidden()
 */
class ArchApps_CustomTabs_Block_Adminhtml_Tab_Page 
    extends Mage_Adminhtml_Block_Template
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    /**
     * Set the template file for the custom cms page tab block
     */
    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('archapps/customtabs/page.phtml');
    }

    /**
     * Whether this tab can be shown. Setting this to false will result
     * in tab not being added at all.
     *
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Whether this tab is hidden. Setting this to true will result in
     * tab being just hidden with "display:none" CSS rule.
     *
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Retrieve tab label
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('archapps_customtabs')->__('Custom Tab');
    }

    /**
     * Retrieve tab title
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('archapps_customtabs')->__('Custom Tab');
    }
}