<?php

class Custom_CmsMenu_Block_Adminhtml_Cmslinks_Edit_Cmslinks extends Mage_Adminhtml_Block_Widget_Form
{
    
    protected function _prepareForm() 
    {
        $helper = Mage::helper('customcmsmenu');
        $model  = Mage::registry('current_cmslinks');

        $form     = new Varien_Data_Form();
        $fieldset = $form->addFieldset('general_form', array(
            'legend' => $helper->__('New Link') 
        ));
        
        $fieldset->addField('label', 'text', array(
            'label'    => $helper->__('Label'),
            
            'required' => true,
            
            'name'     => 'menu_name',
        ));
        
        $fieldset->addField('url_key', 'text', array(
            'label'    => $helper->__('URL Key'),
            'name'     => 'url_key',
            'required' => true,
        ));

        $fieldset->addField('position', 'select', array(
            'label'   => $helper->__('Position'),
            'required' => true,
            'name'    => 'position',
        ));
        
        $fieldset->addField('Type', 'text', array(
            'label'   => $helper->__('URL Key'),
            'name'    => 'url_key',
            'type'    => 'options',
            'options' => array()
        ));
        
        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }
}