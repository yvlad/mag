<?php

class Custom_CmsMenu_Block_Adminhtml_Cmsmenu_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm() 
    {
        $helper = Mage::helper('customcmsmenu');
        $model  = Mage::registry('current_cmsmenu');
        
        $form = new Varien_Data_Form(array(
            'id'     => 'edit_form',
            'action' => $this->getUrl('*/*/save', array(
                'id' => $this->getRequest()->getParam('id')
            )),
            'method'  => 'post',
            'enctype' => 'multipart/form-data'
        ));
        $this->setForm($form);
        
        $config = array(
          'legend' => $helper->__('Menu Information')  
        );
                
        $fieldset = $form->addFieldset('cmsmenu_form', $config);
        
        
        $fieldset->addField('menu_name', 'text', array(
            'label'    => $helper->__('Menu Name'),
            'required' => true,
            'name'     => 'menu_name',
        ));
        
        $fieldset->addField('created', 'hidden', array(
            'name'     => 'created',
        ));

        $fieldset->addField('status', 'select', array(
            'label'   => $helper->__('Status'),
            'default' => 0,
            'name'    => 'status',
            'type'    => 'options',
            'options' => array(
                0 => 'Disable',
                1 => 'Enable'
            )
        ));

       $form->setUseContainer(true);
       
        if($data = Mage::getSingleton('adminhtml/session')->getFormData()){
            $form->setValues($data);
        } else {
            $form->setValues($model->getData());
        }
        return parent::_prepareForm();
    }
}