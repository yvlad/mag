<?php

class Custom_CmsMenu_Block_Adminhtml_Cmsmenu_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    protected function _construct() {
        //инициализируется блок редактирования новости, в конструкторе которого происходит инициализация переменных формы,
        // которые будут использоваться для построения класса самой формы по схеме [_blockGroup]/[_controller]_[_mode]_form, 
        // в результате получится dsnews/adminhtml_news_edit_form (_mode по умолчанию имеет значение edit).
        $this->_blockGroup = 'customcmsmenu';//устанавливаем блок группу
        $this->_controller = 'adminhtml_cmsmenu'; //устанавливаем контроллер для обработки/вывода блока
    }
    
    public function getHeaderText() {
        $helper = Mage::helper('customcmsmenu');//Получаем хелпер
        $model  = Mage::registry('current_cmsmenu');// Получаем из реестра current_cmsmenu глоб переменную
        
        if($model->getId()){
            return $helper->__("Edit Menu item '%s'", $this->escapeHtml($model->getMenuName()));//Получаем вывод в шапке редактирования меню
        }else{
            return $helper->__("New Menu");//Получаем вывод в шапке Добавления меню
        }
    }
}