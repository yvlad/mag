<?php

class Cminds_CmsMenu_Adminhtml_CmsMenuController extends Mage_Adminhtml_Controller_Action
{
	public function indexAction()
	{
            $this->loadLayout();
            $this->_setActiveMenu('cmindscmsmenu');

            $contentBlock = $this->getLayout()->createBlock('cmindscmsmenu/adminhtml_CmsMenu');
          
            $this->_addContent($contentBlock);
            $this->renderLayout();
	}
}

